- [Instalar Sass](#instalar-sass)
- [Variables](#variables)
- [Interpolacion y Nesting](#interpolacion-y-nesting)
  - [Parent Element Sass](#parent-element-sass)
- [Valores en Sass](#valores-en-sass)
  - [Numeros](#numeros)
  - [Division](#division)
  - [String](#string)

## Instalar Sass

Intalar node

configurar `.nvmrc`

```sh
echo echo "lts/fermium" > .nvmrc
```

Instalar `sass`

```scss
npm install sass --save-dev
```

Instalar `sass` de manera global

```sh
npm install sass -g
```

Compilar `sass`

```sh
sass --watch sass:css
```

## Variables 

```scss
$primary: blue;
```

## Interpolacion y Nesting

### Parent Element Sass

```scss
.card {
	color: teal;
    // modificador
    &__title {
        text-align: center;
        &--green  {
            display: grid;
		}
    }
	
    
	&::after {
		color: crimson;
	}
    &::after {
        content: "";
    }
    
    // Referenciar
    :not(&){
        opacity: 0;
    }
}
```

## Valores en Sass

### Numeros

```scss
$variable: 100;
$pixeles: 100px;

@debug 100px*100px;
body {
    animation-duration: 30px + 40px;
}
```

### Division

```scss
// Interpolation module mate
@use 'sass:math'

$variable: 10px / 2px;

body {
	animation-duration: $variable;
    font-size: math.div(25,5) * 2px;
    // string '' ""
    $selector: "h1"
    $strings-comillas: "header header header"
    // unquoted string
    $strings-sincomillas: center;
}
```

### String

```scss
@use 'sass:meta'
    
@debug meta.type-of();

.title {
    text-align: string.unquote($strings-comillas)
}
```

### Listas

```scss
@use 'sass:meta';

$boxshadow: 1px 1px 20px;  // [lista, lista,]
@debug meta.type-of($boxshadow);

.grid {
    margin-top:list.nth($margen, 1);
    margin-bottom: list-nth($margen, 2);
}
```

### Mapas

```scss
$colores: (
    "key": crimson;
    "primary": royalblue,
	"secondary": white;
)
h1 {
    color: map.get($colores, "primary;
}
 
```

### Valores Booleanos

```scss
$condicional: true;

// null, false
@debug $condicional;

@if($condicional){
    .grid{
        display: grid;
    }
} @else {
    .flexbox{
        display: flex;
    }
}

@debug 100px > 100;
```

