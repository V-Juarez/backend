import logging
import requests

# DEBUG = 10
# INFO = 20 
# WARNING = 30
# ERROR = 40
# CRITICAL =50

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(threadName)s - %(levelname)s - %(message)s',
                    filename='service.log',
                    filemode='a')

from requests.models import Response

def get_current_price(id):
    logging.debug("Entramos a la funcion get_current_price")


    response = requests.get(f'https://api.coingecko.com/api/v3/simple/price?ids={id}&vs_currencies=usd')

    if response.status_code == 200:
        logging.info("La respuesta fue exitosa.")

        return response.json()
    else:
        logging.warning("No fue posible obtenenr una respuesta.")

    return None

if __name__ == '__main__':

    response = get_current_price('bitcoin')
    logging.debug('Obtengamos una respuesta')

    logging.info(response)