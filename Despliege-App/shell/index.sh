#!/bin/bash

read -p "Ingresar tu username: " username
read -p "Ingresa tu edad: " edad
# ()
# (( ))
#[[]
# [[ ]]
# string == !=
# enteros > < >= <= ==  !=

# -gt -> >
# -lt -> <
# -ge -> >=
# -le -> <=
# -eq -> ==
# -ne -> !=


if [[ $username == "Cody" ]]; then
	echo "Hola Cody, que gusto de saludarte."
else
	echo "Hola usuario" $username
fi
