<h1>Backend Django</h1>

<h3>Eduardo</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Introducción](#introducción)
  - [¿Por qué Django?](#por-qué-django)
  - [Instalación de Django](#instalación-de-django)
  - [Hola Mundo](#hola-mundo)
  - [Renderizar template](#renderizar-template)
  - [Integración con bootstrap](#integración-con-bootstrap)
  - [Contexto en Django](#contexto-en-django)
  - [Ciclos y condicionales](#ciclos-y-condicionales)
  - [Archivos estaticos](#archivos-estaticos)
  - [Super usuario](#super-usuario)
- [2. Formularios](#2-formularios)
  - [Formulario Login](#formulario-login)
  - [Autenticación](#autenticación)
  - [Redirect](#redirect)
  - [Mensajes](#mensajes)
  - [Condicionar clases](#condicionar-clases)
  - [Logout](#logout)
  - [Formularios Django](#formularios-django)
  - [Estilos para formularios](#estilos-para-formularios)
  - [Iteración de campos](#iteración-de-campos)
  - [Leer datos del formulario](#leer-datos-del-formulario)
  - [Crear usuarios](#crear-usuarios)
  - [Validar campos](#validar-campos)
  - [Validar campos pt2](#validar-campos-pt2)
  - [Método save](#método-save)
- [3. Templates](#3-templates)
  - [Herencia de templates](#herencia-de-templates)
  - [Includes](#includes)
  - [Navbar](#navbar)
  - [Font Awesome](#font-awesome)
  - [Links](#links)
  - [Usuarios autenticados](#usuarios-autenticados)
- [4. Productos](#4-productos)
  - [Modelo de productos](#modelo-de-productos)
  - [Administrador de productos](#administrador-de-productos)
  - [Shell de Django](#shell-de-django)
  - [Listado de productos](#listado-de-productos)
  - [Clase ListView](#clase-listview)
  - [Clase DetailView](#clase-detailview)
  - [Templates de aplicaciones](#templates-de-aplicaciones)
  - [Slug](#slug)
  - [Slugs automáticos](#slugs-automáticos)
  - [Pre save](#pre-save)
  - [Slug Unicos](#slug-unicos)
  - [Imagenes](#imagenes)
  - [Listado de productos](#listado-de-productos-1)
  - [Snippets](#snippets)
  - [Buscador de productos](#buscador-de-productos)
  - [Relación muchos a muchos](#relación-muchos-a-muchos)
  - [Búsqueda por múltiples filtros](#búsqueda-por-múltiples-filtros)
  - [Rutas con argumentos](#rutas-con-argumentos)
  - [Respaldo de información](#respaldo-de-información)
- [5. Usuarios](#5-usuarios)
  - [Proxy Model](#proxy-model)
  - [Relación uno a uno](#relación-uno-a-uno)
  - [Usuarios abstracto](#usuarios-abstracto)
- [6. Carrito de compras](#6-carrito-de-compras)
  - [Modelo carrito de compras](#modelo-carrito-de-compras)
  - [Vista carrito compras](#vista-carrito-compras)
  - [Sesiones](#sesiones)
  - [Crear carrito de compras](#crear-carrito-de-compras)
  - [Id carrito de compras](#id-carrito-de-compras)
  - [Obtener o crear carrito de compras](#obtener-o-crear-carrito-de-compras)
  - [Agregar productos al carrito](#agregar-productos-al-carrito)
  - [Filtros propios](#filtros-propios)
  - [Listado de productos](#listado-de-productos-2)
  - [Eliminar productos del carrito](#eliminar-productos-del-carrito)
  - [Error 404](#error-404)
  - [Calcular total](#calcular-total)
  - [Mostrar subtotal](#mostrar-subtotal)
  - [Cantidad de productos](#cantidad-de-productos)
  - [Formulario de cantidades](#formulario-de-cantidades)
  - [Agregar cantidad](#agregar-cantidad)
  - [Problema n+1 Query](#problema-n1-query)
  - [Actualizar cantidad](#actualizar-cantidad)
  - [Calcular cantidad](#calcular-cantidad)
  - [Filtro de cantidad](#filtro-de-cantidad)
- [7. Orden de compra](#7-orden-de-compra)
  - [Modelo orden de compra](#modelo-orden-de-compra)
  - [Vista de la orden](#vista-de-la-orden)
  - [Crear orden](#crear-orden)
  - [Crear orden Id](#crear-orden-id)
  - [Resumen del pedido](#resumen-del-pedido)
  - [Calcular total](#calcular-total-1)
  - [Propiedades](#propiedades)
  - [Login requerido](#login-requerido)
  - [Redirect](#redirect-1)
  - [Breadcrumb](#breadcrumb)
- [8. Dirección de envío](#8-dirección-de-envío)
  - [Modelo dirección de envío](#modelo-dirección-de-envío)
  - [Vista de direcciones](#vista-de-direcciones)
  - [Formularios basados en modelos](#formularios-basados-en-modelos)
  - [Clases en formularios](#clases-en-formularios)
  - [Crear dirección de envío](#crear-dirección-de-envío)
  - [Lista de direcciones](#lista-de-direcciones)
  - [Restrigir direcciones](#restrigir-direcciones)
  - [Editar direcciones](#editar-direcciones)
  - [Validar peticiones](#validar-peticiones)
  - [Eliminar dirección](#eliminar-dirección)
  - [Dirección principal](#dirección-principal)
- [9. Orden y dirección de envío](#9-orden-y-dirección-de-envío)
  - [Vista de direcciones](#vista-de-direcciones-1)
  - [Relación orden y dirección](#relación-orden-y-dirección)
  - [Crear dirección en envío](#crear-dirección-en-envío)
  - [Seleccionar dirección](#seleccionar-dirección)
  - [Validar eliminación](#validar-eliminación)
- [10. Pedidos](#10-pedidos)
  - [Vista de confirmación](#vista-de-confirmación)
  - [Vista de confirmación pt2](#vista-de-confirmación-pt2)
  - [Cancelar pedido](#cancelar-pedido)
  - [Completar pedido](#completar-pedido)
  - [Envío de correos](#envío-de-correos)
  - [Obtener orden por estatus](#obtener-orden-por-estatus)
  - [Listado de ordenes](#listado-de-ordenes)
  - [Decoradores](#decoradores)
  - [Links externos](#links-externos)
  - [Envío de correos de forma asincrona](#envío-de-correos-de-forma-asincrona)
  - [Métodos y propiedades](#métodos-y-propiedades)
- [11. Código de promoción](#11-código-de-promoción)
  - [Modelo código de promoción](#modelo-código-de-promoción)
  - [Generar código de promoción](#generar-código-de-promoción)
  - [Responder con formato JSON](#responder-con-formato-json)
  - [Petición Asincrona](#petición-asincrona)
  - [Enviar código promocional](#enviar-código-promocional)
  - [Aplicar código promocional](#aplicar-código-promocional)
  - [Actualizar DOM](#actualizar-dom)
  - [Validar código de promoción](#validar-código-de-promoción)
- [12. Métodos de pago](#12-métodos-de-pago)
  - [Modelo método de pago](#modelo-método-de-pago)
  - [Vista método de pago](#vista-método-de-pago)
  - [Integración stripe](#integración-stripe)
  - [Etiquete meta](#etiquete-meta)
  - [Crear cliente](#crear-cliente)
  - [Crear tarjeta](#crear-tarjeta)
  - [Listado de tarjetas](#listado-de-tarjetas)
  - [Orden de compra](#orden-de-compra)
  - [Validaciones breadcrumb](#validaciones-breadcrumb)
- [13. Cobro](#13-cobro)
  - [Modelo Cargo](#modelo-cargo)
  - [Generar cargo](#generar-cargo)
  - [Transacciones](#transacciones)
- [14. Extras](#14-extras)
  - [Conectar con el gestor de base de datos postgresql](#conectar-con-el-gestor-de-base-de-datos-postgresql)
  - [Conectar con el gestor de base de datos MySQL](#conectar-con-el-gestor-de-base-de-datos-mysql)
  - [Paginación](#paginación)
- [15. Clases en vivo](#15-clases-en-vivo)
  - [Relaciones Polimórficas en Django](#relaciones-polimórficas-en-django)

# 1. Introducción

## Introducción
## ¿Por qué Django?
## Instalación de Django

Crear entorno virtual

```sh
python3 -m venv env
```

activar entorno virtual

```sh
source env/bin/activate
```

Instalar django

```sh
pip install Django==2.2.3
```

crear project en django

```sh
django-admin startproject easy_store
```

[<img src="https://i0.wp.com/unaaldia.hispasec.com/wp-content/uploads/2019/06/django.png?resize=1024%2C683&ssl=1" alt="img" style="zoom:10%;" />](https://www.djangoproject.com/download/)

## Hola Mundo

Levantar el servidor

```sh
python3 manage.py runserver
```

Crear el archivo `views.py`

```sh
├── easy_store
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-38.pyc
│   │   ├── settings.cpython-38.pyc
│   │   ├── urls.cpython-38.pyc
│   │   ├── views.cpython-38.pyc
│   │   └── wsgi.cpython-38.pyc
│   ├── settings.py
│   ├── urls.py
│   ├── views.py
│   └── wsgi.py
└── manage.py

```

`views.py`

```python
from django.http import HttpResponse

def index(request):
  return HttpResponse('Hola Mundo, desde el archivo views')
```

`urls.py`

```python
from django.contrib import admin
from django.urls import path

# Import
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('admin/', admin.site.urls),
]

```

## Renderizar template

crear la carpeta y archivo `templates/index`

```sh
└── templates
    └── index.html
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Index</title>
</head>
<body>
  <h1>Hola mundo, desde un archivo HTML</h1>
  <p>
    Este es un texto
  </p>
  <a href="https://codigofacilito.com"></a>
</body>
</html>
```

archivo `settings.py`

```python
TEMPLATES = [
  {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'], # agregar la carpeta templates
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

`views.py`

```python
from django.shortcuts import render
from django.http import HttpResponse

def index(request):
  return render(request, 'index.html', {
    # contexto
    
  })
```

## Integración con bootstrap
## Contexto en Django
## Ciclos y condicionales
## Archivos estaticos
## Super usuario

# 2. Formularios

## Formulario Login
## Autenticación
## Redirect
## Mensajes
## Condicionar clases
## Logout
## Formularios Django
## Estilos para formularios
## Iteración de campos
## Leer datos del formulario
## Crear usuarios
## Validar campos
## Validar campos pt2
## Método save

# 3. Templates

## Herencia de templates
## Includes
## Navbar
## Font Awesome
## Links
## Usuarios autenticados

# 4. Productos

## Modelo de productos
## Administrador de productos
## Shell de Django
## Listado de productos
## Clase ListView
## Clase DetailView
## Templates de aplicaciones
## Slug
## Slugs automáticos
## Pre save
## Slug Unicos
## Imagenes
## Listado de productos
## Snippets
## Buscador de productos
## Relación muchos a muchos
## Búsqueda por múltiples filtros
## Rutas con argumentos
## Respaldo de información

# 5. Usuarios

## Proxy Model
## Relación uno a uno
## Usuarios abstracto

# 6. Carrito de compras

## Modelo carrito de compras
## Vista carrito compras
## Sesiones
## Crear carrito de compras
## Id carrito de compras
## Obtener o crear carrito de compras
## Agregar productos al carrito
## Filtros propios
## Listado de productos
## Eliminar productos del carrito
## Error 404
## Calcular total
## Mostrar subtotal
## Cantidad de productos
## Formulario de cantidades
## Agregar cantidad
## Problema n+1 Query
## Actualizar cantidad
## Calcular cantidad
## Filtro de cantidad

# 7. Orden de compra

## Modelo orden de compra
## Vista de la orden
## Crear orden
## Crear orden Id
## Resumen del pedido
## Calcular total
## Propiedades
## Login requerido
## Redirect
## Breadcrumb

# 8. Dirección de envío

## Modelo dirección de envío
## Vista de direcciones
## Formularios basados en modelos
## Clases en formularios
## Crear dirección de envío
## Lista de direcciones
## Restrigir direcciones
## Editar direcciones
## Validar peticiones
## Eliminar dirección
## Dirección principal

# 9. Orden y dirección de envío

## Vista de direcciones
## Relación orden y dirección
## Crear dirección en envío
## Seleccionar dirección
## Validar eliminación

# 10. Pedidos

## Vista de confirmación
## Vista de confirmación pt2
## Cancelar pedido
## Completar pedido
## Envío de correos
## Obtener orden por estatus
## Listado de ordenes
## Decoradores
## Links externos
## Envío de correos de forma asincrona
## Métodos y propiedades

# 11. Código de promoción

## Modelo código de promoción
## Generar código de promoción
## Responder con formato JSON
## Petición Asincrona
## Enviar código promocional
## Aplicar código promocional
## Actualizar DOM
## Validar código de promoción

# 12. Métodos de pago

## Modelo método de pago
## Vista método de pago
## Integración stripe
## Etiquete meta
## Crear cliente
## Crear tarjeta
## Listado de tarjetas
## Orden de compra
## Validaciones breadcrumb

# 13. Cobro

## Modelo Cargo
## Generar cargo
## Transacciones

# 14. Extras

## Conectar con el gestor de base de datos postgresql
## Conectar con el gestor de base de datos MySQL
## Paginación

# 15. Clases en vivo

## Relaciones Polimórficas en Django