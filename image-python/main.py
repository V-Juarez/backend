#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image

if __name__ == '__main__':
    
    try:
        image = Image.open('img/cyberpunk.jpg')

        print(image.size) # tupla (w, h)

        width, height = image.size

        print('Ancho', width)
        print('Alto', height)

        print( image.mode )
        print( image.format )

        # image.show()
    except FileNotFoundError as error:
        print('No es posible completar la operacion!')